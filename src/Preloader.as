package 
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Preloader extends MovieClip 
	{
		
		[Embed(source = "Resource/Splinter-Teenage-Mutant-Ninja-Turtles.jpg")] private var zl: Class;
		private var sh: Shape;
		
		
		private function DrawP(proc: int)
		{
			sh.graphics.clear();
			sh.graphics.beginFill(0xFFFFFF, 1);
			sh.graphics.lineStyle(2, 0x000000, 1);
			sh.graphics.drawRect(120, 350, 300, 20);
			sh.graphics.endFill();
			sh.graphics.beginFill(0x00FF00, 1);
			sh.graphics.lineStyle(null,null,0);
			sh.graphics.drawRect(122, 352, 296 * proc / 100, 16);
			sh.graphics.endFill();
		}
		
		public function Preloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			var d: DisplayObject = new zl();
			this.addChild(d);
			sh = new Shape();
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			this.addChild(sh);
			
			// TODO show loader
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			DrawP(loaderInfo.bytesLoaded / loaderInfo.bytesTotal * 100);
			// TODO update loader
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO hide loader
			
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
	}
	
}