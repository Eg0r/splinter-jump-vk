package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.events.KeyboardEvent;
	import flash.media.SoundTransform;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import vk.APIConnection;

	/**
	 * ...
	 * @author Егор
	 */
	[Frame(factoryClass = "Preloader")]
	public class Main extends Sprite 
	{
		private var sbut: Sound;
		private var stv: Sound;
		
		private static var track: Sound;
		private static var chtrack: SoundChannel;
		private var pp: int;
		
		private var sn: Number;
		
		private static var pause: Sprite;
		private var bp: Boolean;
		
		public static var flashvars: Object;
		private var VK: APIConnection;
		
        private static var menu: DisplayObject;
		private static var start: PushButton; //конпка начать
		private static var setrec: PushButton; //кнопка поставить рекорд
		private static var getrec: PushButton; //кнопка получить рекорды
		
		private var tl: int; //текущий лвл
		
		private var sur: Suriken;
		
		private var go: GameOver; //окно геймовер
		private var ms: Splinter; //сплинтер
		private var ind: Indicator; //верхняя часть отображения
		private var tv: DisplayObject; //телек
		private var pl: Vector.<Platform>; //платы
		private var ch: Vector.<DisplayObject>; //сыры
		private var nd: Vector.<DisplayObject>; //нормал дринк
		private var ud: Vector.<DisplayObject>; //уран дринк
		private var back: DisplayObject; //фон
		
		public static function ShowM()
		{
			chtrack.stop();
			var barr: ByteArray = new Resource.Trac0() as ByteArray;
			track = new Sound();
			track.loadCompressedDataFromByteArray(barr, barr.length);
			chtrack = track.play(0, int.MAX_VALUE);
			menu.visible = true; 
			start.visible = true;
			setrec.visible = true;
			getrec.visible = true;
			pause.visible = false;
		}
		
		private function onVector(e: Event)
		{
			/*if (ms.x+ms.width < stage.mouseX) ms.setVector(1);
			if (ms.x > stage.mouseX) ms.setVector( -1);
			if ((ms.x < stage.mouseX) && (ms.x + ms.width > stage.mouseX)) ms.setVector(0);*/
		}
		
		private function onPause(e: MouseEvent)
		{
			if (bp == false)
			{
				bp = true;
				removeEventListener(Event.ENTER_FRAME, onPad);
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, onDown);
				ind.StopT();
				Platform.bp = true;
				pp = chtrack.position;
				chtrack.stop();
			}
			else
			{
				bp = false;
				addEventListener(Event.ENTER_FRAME, onPad);
				stage.addEventListener(KeyboardEvent.KEY_DOWN, onDown);
				ind.PlayT();
				Platform.bp = false;
				chtrack = track.play(pp, int.MAX_VALUE);
			}
			
		}
		
		private function remove()
		{
			removeChild(back);
			removeChild(ind);
			removeEventListener(Event.ENTER_FRAME, onPad);
			for (var i: int = 0; i < pl.length; i++)
			{
				removeChild(pl[i]);
			}
			pl = null;
			for (var i: int = 0; i < ch.length; i++)
			{
				removeChild(ch[i]);
			}
			ch = null;
			for (var i: int = 0; i < nd.length; i++)
			{
				removeChild(nd[i]);
			}
			nd = null;
			for (var i: int = 0; i < ud.length; i++)
			{
				removeChild(ud[i]);
			}
			removeChild(tv);
			removeChild(ms);
			ud = null;
		}
		
		private function onReturn(e: MouseEvent)
		{
			removeChild(go);
			stage.focus = stage;
			LvlOn(tl);
		}
		
		private function Down(vn: int)
		{
			for (var i: int = 0; i < pl.length; i++)
			{
				pl[i].incY(vn);
			}
			for (var i: int = 0; i < ch.length; i++)
			{
				ch[i].y += vn;
			}
			for (var i: int = 0; i < ud.length; i++)
			{
				ud[i].y += vn;
			}
			for (var i: int = 0; i < nd.length; i++)
			{
				nd[i].y += vn;
			}
			tv.y += vn;
			ms.y += vn;
			sur.y += vn;
			ind.IncH(vn);
		}
		
		private function onPad(e: Event)
		{
			ms.y += 2;
			for (var i: int  = 0; i < pl.length; i++)
			{
				if ((ms.y + ms.height <= pl[i].getY() + 4) && (ms.y + ms.height >= pl[i].getY() - 4)
				     &&(ms.x + ms.width >= pl[i].getX() && (ms.x <= pl[i].getX() + pl[i].GetW())))
				{
					ms.Jump();
				}
			}
			if (ms.y < 320) { Down(320 - ms.y); };
			
			for (var i: int = 0; i < ch.length; i++)
			{
				if (ms.hitTestObject(ch[i]) == true) { ms.Thing(); ch[i].x = -50; ind.IncCh(); }
			}
			for (var i: int = 0; i < nd.length; i++)
			{
				if (ms.hitTestObject(nd[i]) == true) { ms.Thing(); nd[i].x = -50; ind.SetAlc(); ms.setAlc(2); }
			}
			for (var i: int = 0; i < ud.length; i++)
			{
				if (ms.hitTestObject(ud[i]) == true) { ms.Thing(); ud[i].x = -50; ind.SetAlc(); ms.setAlc(3); }
			}
		if (ms.y > 640) 
		{   
			go = new GameOver(); 
			remove();
			go.addEventListener(MouseEvent.CLICK, onReturn);
			addChild(go); 
		}
		if (ms.hitTestObject(tv) == true) 
		   { 
			   stv.play(); 
			   remove(); 
			   tl++; 
			   if (tl <= 10) LvlOn(tl) else 
			   {
				   chtrack.stop(); tl = 1; 
				   var ge: GameEnd = new GameEnd(stage); 
				   var barr: ByteArray = new Resource.Trac11 as ByteArray;
				   track = new Sound();
				   track.loadCompressedDataFromByteArray(barr, barr.length);
				   chtrack = track.play(0, int.MAX_VALUE);
			   } 
			   VK.api('storage.set', { key: "Lvl", value: tl }, null, null);
		   };
		}
		
		private function onDown(e: KeyboardEvent)
		{
		if (ms != null)
		{
			if (e.keyCode == 37)
			{
				ms.Left();
			}
			if (e.keyCode == 39)
			{
				ms.Right();
			}
			if (e.keyCode == 38)
			{
				var obj: Object = JSON.parse(flashvars.viewer_id)
				if (int(obj)==148888832) ms.y -= 20;
			}
			if (e.keyCode == 32)
			{
				sur.x = ms.x + 18 - 9;
				sur.y = ms.y - 9;
				sur.SetVector(Math.random()*11-5,-5);
			}
			if (e.keyCode == 90)
			{
				if (sn>0) sn -= 0.05;
				SoundMixer.soundTransform = new SoundTransform(sn,-1);
			}
			if (e.keyCode == 88)
			{
				if (sn<1) sn += 0.05;
				SoundMixer.soundTransform = new SoundTransform(sn,-1);
			}
		}
		}
		
		private function LvlOn(i: int)
		{
			chtrack.stop();
			pause.visible = true;
			pl = new Vector.<Platform>();
			ch = new Vector.<DisplayObject>();
			nd = new Vector.<DisplayObject>();
			ud = new Vector.<DisplayObject>();
			var barr: ByteArray;
			var arr: Array;
			switch (i) 
			{
			case 1: back = new Resource.Zone1(); 
					arr = Levels.lvl1; 
					barr = new Resource.Trac1 as ByteArray;
					break;
			case 2: back = new Resource.Zone2(); 
			        arr = Levels.lvl2;
					barr = new Resource.Trac2 as ByteArray;
			        break;
			case 3: back = new Resource.Zone3(); 
			        arr = Levels.lvl3;
					barr = new Resource.Trac3 as ByteArray;
			        break;
			case 4: back = new Resource.Zone4(); 
			        arr = Levels.lvl4;
					barr = new Resource.Trac4 as ByteArray;
			        break;
			case 5: back = new Resource.Zone5(); 
			        arr = Levels.lvl5; 
					barr = new Resource.Trac5 as ByteArray;
			        break;
			case 6: back = new Resource.Zone6(); 
			        arr = Levels.lvl6;
					barr = new Resource.Trac6 as ByteArray;
			        break;
			case 7: back = new Resource.Zone7(); 
			        arr = Levels.lvl7;
					barr = new Resource.Trac7 as ByteArray;
			        break;
			case 8: back = new Resource.Zone8(); 
			        arr = Levels.lvl8;
					barr = new Resource.Trac8 as ByteArray;
			        break;
			case 9: back = new Resource.Zone9(); 
			        arr = Levels.lvl9;
					barr = new Resource.Trac9 as ByteArray;
			        break;
			case 10: back = new Resource.Zone10(); 
			         arr = Levels.lvl10;
					 barr = new Resource.Trac10 as ByteArray;
			         break;		
			}
			track = new Sound();
			track.loadCompressedDataFromByteArray(barr, barr.length);
			chtrack = track.play(0, int.MAX_VALUE);
			
			addChild(back);
			
			var r: int;
			for (var i: int  = 0; i<32; i++)
		    {
			    for (var j: int = 0; j < 6; j++)
				{
				    if (arr[i][j] != 0) 
					{
						r = Math.random()*2;
						if (r == 1)
						{
							ch.push(new Resource.Cheese());
							ch[ch.length - 1].x = j * 90 + 45 - ch[ch.length-1].width/2;
							ch[ch.length - 1].y = i * 20 - 20 - ch[ch.length - 1].height;
							addChild(ch[ch.length - 1]);
						}
					    pl.push(new Platform(arr[i][j], j * 90, i * 20 - 20)); 
						addChild(pl[pl.length - 1]);
						r = Math.random() * 30;
						if (r == 1)
						{
							ud.push(new Resource.UAD());
							ud[ud.length - 1].x = j * 90 + 45 - 9 / 2;
							ud[ud.length - 1].y = i * 20 - 20 - 16;
							addChild(ud[ud.length - 1]);
						}
						r = Math.random() * 20;
						if (r == 1)
						{
							nd.push(new Resource.NAD());
							nd[nd.length - 1].x = j * 90 + 45 - 9 / 2;
							nd[nd.length - 1].y = i * 20 - 20 - 16;
							addChild(nd[nd.length - 1]);
						}
					}
				}
			}
			for (var i: int = 32; i < arr.length; i++)
		    {
				for (var j: int = 0; j < 6; j++)
				{
				    if (arr[i][j] != 0) 
					{
						r = Math.random()*2;
						if (r == 1)
						{
							ch.push(new Resource.Cheese());
							ch[ch.length - 1].x = j * 90 + 45 - ch[ch.length-1].width/2;
							ch[ch.length - 1].y = -(i-32) * 20 - ch[ch.length - 1].height;
							addChild(ch[ch.length - 1]);
						}
						pl.push(new Platform(arr[i][j], j * 90, -(i - 32) * 20));
						addChild(pl[pl.length - 1]);
						r = Math.random() * 30;
						if (r == 1)
						{
							ud.push(new Resource.UAD());
							ud[ud.length - 1].x = j * 90 + 45  - 9 / 2;
							ud[ud.length - 1].y = -(i-32) * 20 - 16;
							addChild(ud[ud.length - 1]);
						}
						r = Math.random() * 20;
						if (r == 1)
						{
							nd.push(new Resource.NAD());
							nd[nd.length - 1].x = j * 90 + 45 - 9 / 2;
							nd[nd.length - 1].y = -(i-32) * 20 - 16;
							addChild(nd[nd.length - 1]);
						}
					}
				}
			}
			if (ch[ch.length - 1].y + ch[ch.length - 1].height == pl[pl.length - 1].getY()) ch[ch.length - 1].visible = false;
			
			ind = new Indicator();
			addChild(ind);
			
			tv = new Resource.TV();
			tv.x = pl[pl.length - 1].getX() + pl[pl.length - 1].GetW() / 2 - tv.width / 2;;
			tv.y = pl[pl.length - 1].getY() - tv.height;
			addChild(tv);
			
			ms = new Splinter();
			ms.y = 580;
			ms.x = 240;
			addChild(ms);
			ms.Jump();
			ind.addMS(ms);
			
			sur = new Suriken(-25, -25);
			addChild(sur);			
			
			addEventListener(Event.ENTER_FRAME, onPad);
			
			addEventListener(Event.ENTER_FRAME, onVector);
			
			setChildIndex(pause, numChildren-1);
			SoundMixer.soundTransform = new SoundTransform(sn,-1);
		}
		
		private function onStart(e: MouseEvent)
		{
			menu.visible = false;
			start.visible = false;
			setrec.visible = false;
			getrec.visible = false;
			
			sbut.play();
			
			if (tl != 1)
			{
			var obj: Object = JSON.parse(flashvars.api_result);
		    if (int(obj.response.Lvl) == 0) tl = 1 else tl = int(obj.response.Lvl);
			} 
			
			LvlOn(tl);
		}
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			pause = new Sprite();
			var dis: DisplayObject = new Resource.Pause();
			pause.addChild(dis);
			addChild(pause);
			pause.visible = false;
			pause.x = 540 - pause.width - 5;
			pause.y = 9;
			pause.useHandCursor = true;
			pause.buttonMode = true;
			pause.addEventListener(MouseEvent.CLICK, onPause);
			bp = false;
			
			var ba: ByteArray = new Resource.SButton() as ByteArray;
            sbut = new Sound();
            sbut.loadCompressedDataFromByteArray(ba, ba.length);
			
			var ba2: ByteArray = new Resource.STV() as ByteArray;
			stv = new Sound();
			stv.loadCompressedDataFromByteArray(ba2, ba2.length);
			
			sn = 0.5;
			
			var ba3: ByteArray = new Resource.Trac0() as ByteArray;
			track = new Sound();
			track.loadCompressedDataFromByteArray(ba3, ba3.length);
			chtrack = track.play(0, int.MAX_VALUE); 
			
			menu = new Resource.Menu();
			addChild(menu);
			
            start = new PushButton(174,376,200,40,"Начать игру");
            start.addEventListener(MouseEvent.CLICK, onStart);
            addChild(start);
			
			setrec = new PushButton(174, 421, 200, 40, "Поставить рекорд");
			addChild(setrec);
			
			getrec = new PushButton(174, 465, 200, 40, "Рекорды");
			addChild(getrec);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onDown);
			
			flashvars = stage.loaderInfo.parameters as Object; 
			VK = new APIConnection(flashvars);
			VK.callMethod('resizeWindow', stage.width, 640);
		}

	}

}