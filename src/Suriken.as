package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Suriken extends Sprite
	{
		private var s1: DisplayObject;
		private var s2: DisplayObject;
		private var s: int;
		
		private var strike: Sound;
		
		private var vect: Array;
		
		private function onVector(e: Event)
		{
			this.x += vect[0];
			this.y += vect[1];
			if ((this.x<-this.width)||(this.x>560)) removeEventListener(Event.ENTER_FRAME, onVector);
		}
		
		public function SetVector(x: int, y: int)
		{
			vect[0] = x;
			vect[1] = y;
			strike.play();
			this.addEventListener(Event.ENTER_FRAME, onVector); 
		}
		
		private function onKr(e: Event)
		{			
			s++;
			if (s == 3) 
			{ s = 0; 
			    if (s2.visible == false) 
			    { 
				   s2.visible = true; s1.visible = false; 
		        }
			    else
			    {
			       s2.visible = false; s1.visible = true;	
			    }
			}	
		}
		
		public function Suriken(x: int, y: int)
		{
			super();
			
			vect = new Array(2);
			
			s1 = new Resource.S1();
			s2 = new Resource.S2();
			
			var ba: ByteArray = new Resource.SSurikenStrike() as ByteArray;
			strike = new Sound();
			strike.loadCompressedDataFromByteArray(ba, ba.length);
			
			this.addChild(s1);
			this.x = x;
			this.y = y;
			s2.visible = false;
			this.addChild(s2);
			s = 0;
			this.addEventListener(Event.ENTER_FRAME, onKr);
		}	
	}
	
}	