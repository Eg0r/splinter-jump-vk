package {

import flash.display.Sprite;
import flash.display.DisplayObject;
import flash.display.SimpleButton;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class PushButton extends SimpleButton
{
	
 private var button_x:Number=0;
 private var button_y:Number=0;
 private var button_width:Number=0;
 private var button_height:Number=0;
 private var button_text:String="";

 public function PushButton(buttonX:Number, 
                            buttonY:Number, 
                            buttonWidth:Number, 
                            buttonHeight:Number, 
                            buttonText:String)
 {	 
  button_x=buttonX;
  button_y=buttonY;
  button_width=buttonWidth;
  button_height=buttonHeight;
  button_text=buttonText;
 
  upState=button_sprite(0x008000);
  overState=button_sprite(0x90EE90);
  downState=button_sprite(0x90EE90);
  hitTestState=button_sprite(0xBBBBBB);
 
  x=buttonX;
  y=buttonY;
 }


 private function button_sprite(color:uint = 0x888888):Sprite
 {

  var b_sprite:Sprite=new Sprite();
  
  b_sprite.graphics.lineStyle(1);
  b_sprite.graphics.beginFill(color);
  b_sprite.graphics.drawRect(0, 0, button_width,  button_height);
  b_sprite.graphics.endFill();

  var button_label:TextField;
  button_label=new TextField();
  button_label.text=button_text;
  button_label.selectable = false;
  button_label.autoSize = TextFieldAutoSize.CENTER;
  button_label.scaleY = 1.9;
  button_label.x=(button_width-button_label.textWidth)/2-1;
  button_label.y=(button_height-button_label.textHeight)/2-2;

  b_sprite.addChild(button_label);
 
  return b_sprite;
 }
 
}

}