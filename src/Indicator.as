package 
{
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Indicator extends Sprite 
	{
		private var bl: DisplayObject;
		
		private var spl: Splinter;
		
		private var hp: int;
		private var mut: int;
		private var sch: int;
		
		private var msface: Sprite;
		private var f1: DisplayObject;
		
		private var msec: int;
		private var currdate: Date;
		
		private var ch: TextField; private var chh: int;
		private var h: TextField; private var hh: int;
		private var t: TextField; private var th: int;
		
		private function Graphics(w1: int, w2: int)
		{
			msface.graphics.clear();
			
			msface.graphics.beginFill(0xFFFFFF, 1);
			msface.graphics.lineStyle(null, 0x000000);
			msface.graphics.drawRect(30, 0, 44, 29);
			msface.graphics.endFill();
			msface.graphics.beginFill(0xFF0000, 1);
			msface.graphics.lineStyle(null, 0xFF0000);
			msface.graphics.drawRect(31, 1, w1, 13);
			msface.graphics.endFill();
			msface.graphics.beginFill(0x90EE90, 1);
			msface.graphics.lineStyle(null, 0x90EE90);
			msface.graphics.drawRect(31, 15, w2, 13);
			msface.graphics.endFill();
		}
		
		public function addMS(ms: Splinter)
		{
			spl = ms;
		}
		
		private function onAlc(e: Event)
		{
			sch++;
			if (sch == 10) { sch = 0; mut--; Graphics(hp, mut); if (mut == 0) { removeEventListener(Event.ENTER_FRAME, onAlc); spl.setAlc(1); }  }
		}
		
		public function SetAlc()
		{
			mut = 42;
			sch = 0;
			addEventListener(Event.ENTER_FRAME, onAlc);
		}
		
		public function IncCh()
		{
			chh++;
			ch.text = "Сыр: " + chh;
		}
		
		public function IncH(nh: int)
		{
			hh += nh;
			h.text = "Высота: " + hh.toString();
		}
		
		public function onTime(e: Event)
		{
			msec += 1000 / 40;
			currdate = new Date(msec);
			t.text = "Время: " + currdate.getMinutes() + ":" + currdate.getSeconds() + ":" + Math.round(currdate.getMilliseconds()/10);
		}
		
		public function StopT()
		{
			this.removeEventListener(Event.ENTER_FRAME, onTime);
		}
		
		public function PlayT()
		{
			this.addEventListener(Event.ENTER_FRAME, onTime);
		}
		
		public function Indicator()
		{
			super();
			bl = new Resource.Block();
			bl.width = 540;
			this.addChild(bl);
			ch = new TextField();
			ch.textColor = 0xFFFF00;
			ch.text = "Сыр: 0";
			this.addChild(ch);
			ch.x = 100;
			ch.y = 5;
			ch.scaleX = ch.scaleY = 17 * 4 / 3 / 12;
			
			h = new TextField();
			h.textColor = 0x0000FF;
			h.text = "Высота: 0";
			h.x = 190;
			h.y = 5;
			h.scaleX = h.scaleY = 17 * 4 / 3 / 12;
			this.addChild(h);
			
			t = new TextField();
			t.textColor = 0xFF8000;
			t.text = "Время: 0:0:0";
			t.x = 340;
			t.y = 5;
			t.scaleX = t.scaleY = 17 * 4 / 3 / 12;
			this.addChild(t);
			
			chh = hh = th = 0;
			
			msec = 0;
			this.addEventListener(Event.ENTER_FRAME, onTime);
			
			msface = new Sprite();
			f1 = new Resource.SF1();
			msface.addChild(f1);
			this.addChild(msface);
			msface.x = 20;
			msface.y = 3;
			
			hp = 42;
			mut = 0;
			Graphics(hp, mut);
		}
	}
	
}