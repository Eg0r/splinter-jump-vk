package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Splinter extends Sprite 
	{
		private var jump: Sound;
		private var thing: Sound;
		
		private var vec: int;
		
		private var IsJumping: Boolean;
		
		private var l: DisplayObject;
		private var r: DisplayObject;
		
		private var type: int;
		private var jumpc: int;
		private const jumpt1 = 17;
		private const jumpt2 = 21;
		private const jumpt3 = 26;
		
		public function setVector(e: int)
		{
			vec = e;
		}
		
		private function onMove(e: Event)
		{
			if (vec > 0) this.Right();
			if (vec < 0) this.Left();
		}
		
		public function Thing()
		{
			thing.play();
		}
		
		public function Left()
		{
			if (this.x < 0)
			{
				this.x = 540;
			}
			this.x -= 8;
			r.visible = false;
			l.visible = true;
		}
		
		public function setAlc(t: int)
		{
			type = t;
		}
		
		private function HelpJump(e: Event)
		{
			switch (type)
			{
				case 1: this.y += -(jumpt1 - Math.abs(jumpc - jumpt1)); break;
				case 2: this.y += -(jumpt2 - Math.abs(jumpc - jumpt2)); break;
				case 3: this.y += -(jumpt3 - Math.abs(jumpc - jumpt3)); break;
			}
			jumpc--;
			if (jumpc == -1) { this.removeEventListener(Event.ENTER_FRAME, HelpJump); IsJumping = false; }
		}
		
		public function Jump()
		{
		if (IsJumping == false)
		{
		IsJumping = true;	
			switch (type)
			{
				case 1: jumpc = 17; break;
				case 2: jumpc = 21; break;
				case 3: jumpc = 26; break;
			}
			jump.play();
			this.addEventListener(Event.ENTER_FRAME, HelpJump);
		}	
		}
		
		public function Right()
		{
			if (this.x > 540)
			{
				this.x = -this.width;
			}
			this.x += 8;
			l.visible = false;
			r.visible = true;
		}
		
		public function Splinter()
		{
			super();
			
			var bytearray: ByteArray = new Resource.SJump() as ByteArray;
			jump = new Sound();
			jump.loadCompressedDataFromByteArray(bytearray, bytearray.length);
			
			var bytearray2: ByteArray = new Resource.SThing() as ByteArray;
			thing = new Sound();
			thing.loadCompressedDataFromByteArray(bytearray2, bytearray2.length);
			
			type = 1;
			
			l = new Resource.MSL();
			r = new Resource.MSR();
			
			l.visible = false;
			
			IsJumping = false;
			
			this.addChild(l);
			this.addChild(r);
			vec = 0;
			
			this.addEventListener(Event.ENTER_FRAME, onMove);
			
		}
	}
	
}