package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Platform extends Sprite
	{
		private var type: int;
		public var d: DisplayObject;
		public static var bp: Boolean;
		private var s: int;
		private var sk: int;
		private var vect: int;
		
		private function onOrange(e: Event)
		{
		if (bp == false)
		{
			s++;
			if (s == sk) { if (vect == 1) 
			               { 
							   //d.scaleX = 0.5; 
							   d.width = 20;
							   d.height = 90;
							   d.x += 35;
							   d.y -= 35;
							   vect = 0; 
						   } else 
						   { 
							   //d.scaleX = 1; 
							   d.width = 90;
							   d.height = 20;
							   d.x -= 35;
							   d.y += 35;
							   vect = 1; 
						   }; s = 0;/* sk = Math.random() * 12 + 108;*/ }
		}				   
		}
		
		private function onRed(e: Event)
		{
		if (bp == false)
		{
			s++;
			d.y += vect;
			if (s == sk) { vect *= -1; s = 0; /*sk = Math.random() * 20 + 20;*/ };
		}	
		}
		
		private function onBlue(e: Event)
		{
		if (bp == false)
		{
			d.x -= 1;
			if (d.x + d.width <= 0) d.x = 540;
		}
		}
		
		private function onPurple(e: Event)
		{
		if (bp == false)
		{
			d.x += 1;
			if (d.x >= 540) d.x = -d.width;
		}
		}
		
		public function GetW(): int 
		{
			return d.width;
		}
		
		public function getY(): int
		{
			return d.y;
		}
		
		public function getX(): int
		{
			return d.x;
		}
		
		public function incY(ny: int)
		{
			d.y += ny;
		}
		
	    public function Platform(i: int, x: int, y: int)
		{
			super();
			switch (i)
			{
				case 1: d = new Resource.green(); break;
				case 2: d = new Resource.blue(); d.addEventListener(Event.ENTER_FRAME, onBlue); break;
				case 3: d = new Resource.purple(); d.addEventListener(Event.ENTER_FRAME, onPurple); break;
				case 4: d = new Resource.red(); d.addEventListener(Event.ENTER_FRAME, onRed); s = 0; sk = Math.random() * 20 + 20; vect = 2; break;
				case 5: d = new Resource.orange(); d.addEventListener(Event.ENTER_FRAME, onOrange); s = 0; sk = Math.random() * 12 + 108; vect = 1; break;
			}
			type = i;
			d.x = x;
			d.y = y;
			
			bp = false;
			
			this.addChild(d);
		}
	}
}
	