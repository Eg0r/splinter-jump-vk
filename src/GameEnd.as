package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class GameEnd extends Sprite 
	{
		private var p: Vector.<DisplayObject>;
		private var t: int;
		private var spr: Stage;
		private var timer: Timer;
		
		private var text: TextField;
		
		private function onTime(e: TimerEvent)
		{
			this.removeChild(p[t]);
			t++;
			switch (t)
			{
				case 1: text.text = '- Хммм.... - МС'; break;
				case 2: text.x = 20; text.text = '- Я не помню, помню лишь то, \n   что потом я проснулся, - МС'; break;
				case 3: text.x = 70; text.text = '- Понятно всё, он гонит, - ЧН';  break;
				case 4: text.text = ' - Ничё я не гоню, - МС'; break;
				case 5: text.x = 20; text.text = ' - Ещё скажите, что вы не верите \n   в чудо напиток, - МС';  break;
				case 6: text.x = 0; text.text = ' - Без этого напитка Сплинтер бы не упрыгал \n   так далеко, - Эйприл'; break;
				case 7: text.x = 20; text.text = ' - Хм.... Ну возможно история правдивая, - ЧН';  break;
				case 8: text.x = 70; text.text = ' - Гыгыгыгыгы, - МС';  break;
			}
		    if (t == 9) 
			{ 
				timer.stop(); timer.removeEventListener(TimerEvent.TIMER, onTime); 
				this.removeChild(text); spr.removeChild(this);
				Main.ShowM();
			}
		}
		
		public function GameEnd(st: Stage)
		{			
			p = new Vector.<DisplayObject>(9);
			p[0] = new Resource.GE1();
			p[1] = new Resource.GE2();
			p[2] = new Resource.GE3();
			p[3] = new Resource.GE4();
			p[4] = new Resource.GE5();
			p[5] = new Resource.GE6();
			p[6] = new Resource.GE7();
			p[7] = new Resource.GE8();
			p[8] = new Resource.GE9();
			
			t = 0;
			
			for (var i: int = 8; i >= 0; i--)
			{
				this.addChild(p[i]);
			}
			
			spr = st;
			spr.addChild(this);
			
			text = new TextField();
			text.width = 540;
			this.addChild(text);
			text.textColor = 0xFFFFFF;
			text.scaleX = text.scaleY = 15 * 4 / 3 / 12;
			text.text = '- И что же было дальше, учитель? - ЧН';
			text.x = 70;
			text.y = 570;
			
			timer = new Timer(4000);
			timer.addEventListener(TimerEvent.TIMER, onTime);
			timer.start();
		}
	}
	
}