package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Егор
	 */
	public class GameOver extends Sprite 
	{
		private var pad: Sound;
		private var go: DisplayObject;
		private var gotex: TextField;
		
		public function GameOver()
		{
			super();
			
			var ba: ByteArray = new Resource.SPad() as ByteArray;
			pad = new Sound();
			pad.loadCompressedDataFromByteArray(ba, ba.length);
			
			go = new Resource.gameO();
			this.addChild(go);
			this.buttonMode = true;
			
			gotex = new TextField();
			gotex.textColor = 0x00FF00;
			gotex.text = "Game Over";
			gotex.x = 77;
			gotex.y = 256;
			gotex.scaleX = gotex.scaleY = 56 * 4 / 3 / 12;
			this.addChild(gotex);
			pad.play();
		}
	}
	
}