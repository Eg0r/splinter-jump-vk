package 
{
	
	/**
	 * ...
	 * @author Егор
	 */
	public class Resource 
	{
		[Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Pictures/Menu.jpg")] public static var Menu: Class;
		
		[Embed(source = "Resource/Pictures/Block.png")] public static var Block: Class;
		
		[Embed(source = "Resource/Things/Cheese.png")] public static var Cheese: Class;
		[Embed(source = "Resource/Things/TV.png")] public static var TV: Class;
		
		[Embed(source = "Resource/Things/NormalAlepahtoDrink.png")] public static var NAD: Class;
		[Embed(source = "Resource/Things/UranAlepahtoDrink.png")] public static var UAD: Class;
		
		[Embed(source = "Resource/Splinter/Suriken1.png")] public static var S1: Class;
		[Embed(source = "Resource/Splinter/Suriken2.png")] public static var S2: Class;
		
		[Embed(source = "Resource/Splinter/SplinterFace1.png")] public static var SF1: Class; 
		
		[Embed(source = "Resource/Pictures/GameOver.jpg")] public static var gameO: Class;
		
		[Embed(source = "Resource/Splinter/MSL.png")] public static var MSL: Class;
		[Embed(source = "Resource/Splinter/MSR.PNG")] public static var MSR: Class;
		
		[Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Platform/Green.PNG")] public static var green: Class;
     	[Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Platform/Blue.png")] public static var blue: Class;
    	[Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Platform/Purple.png")] public static var purple: Class;
	    [Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Platform/Red.png")] public static var red: Class;
	    [Embed(source = "C:/Users/1/Desktop/Splinter Jump Project/src/Resource/Platform/Orange.png")] public static var orange: Class;
		
		[Embed(source = "Resource/Back/Zone1.jpg")] public static var Zone1: Class;
		[Embed(source = "Resource/Back/Zone2.jpg")] public static var Zone2: Class;
		[Embed(source = "Resource/Back/Zone3.jpg")] public static var Zone3: Class;
		[Embed(source = "Resource/Back/Zone4.jpg")] public static var Zone4: Class;
		[Embed(source = "Resource/Back/Zone5.jpg")] public static var Zone5: Class;
		[Embed(source = "Resource/Back/Zone6.jpg")] public static var Zone6: Class;
		[Embed(source = "Resource/Back/Zone7.jpg")] public static var Zone7: Class;
		[Embed(source = "Resource/Back/Zone8.jpg")] public static var Zone8: Class;
		[Embed(source = "Resource/Back/Zone9.jpg")] public static var Zone9: Class;
		[Embed(source = "Resource/Back/Zone10.jpg")] public static var Zone10: Class;
		
		[Embed(source = "Resource/Sounds/Tune/Button.mp3", mimeType = "application/octet-stream")] public static var SButton: Class;
		[Embed(source = "Resource/Sounds/Tune/Jump.mp3", mimeType = "application/octet-stream")] public static var SJump: Class;
		[Embed(source = "Resource/Sounds/Tune/Pad.mp3", mimeType = "application/octet-stream")] public static var SPad: Class;
		[Embed(source = "Resource/Sounds/Tune/SurikenStrike.mp3", mimeType = "application/octet-stream")]public static var SSurikenStrike: Class;
		[Embed(source = "Resource/Sounds/Tune/Thing.mp3", mimeType = "application/octet-stream")] public static var SThing: Class;
		[Embed(source = "Resource/Sounds/Tune/TV.mp3", mimeType = "application/octet-stream")] public static var STV: Class;
		
		[Embed(source = "Resource/Sounds/Music сжатые/Track0.mp3", mimeType = "application/octet-stream")] public static var Trac0: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track1.mp3", mimeType = "application/octet-stream")] public static var Trac1: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track2.mp3", mimeType = "application/octet-stream")] public static var Trac2: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track3.mp3", mimeType = "application/octet-stream")] public static var Trac3: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track4.mp3", mimeType = "application/octet-stream")] public static var Trac4: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track5.mp3", mimeType = "application/octet-stream")] public static var Trac5: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track6.mp3", mimeType = "application/octet-stream")] public static var Trac6: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track7.mp3", mimeType = "application/octet-stream")] public static var Trac7: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track8.mp3", mimeType = "application/octet-stream")] public static var Trac8: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track9.mp3", mimeType = "application/octet-stream")] public static var Trac9: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track10.mp3", mimeType = "application/octet-stream")] public static var Trac10: Class;
		[Embed(source = "Resource/Sounds/Music сжатые/Track11.mp3", mimeType = "application/octet-stream")] public static var Trac11: Class;
		
		
		[Embed(source = "Resource/Pictures/01.jpg")] public static var GE1: Class;
		[Embed(source = "Resource/Pictures/02.jpg")] public static var GE2: Class;
		[Embed(source = "Resource/Pictures/03.jpg")] public static var GE3: Class;
		[Embed(source = "Resource/Pictures/04.jpg")] public static var GE4: Class;
		[Embed(source = "Resource/Pictures/05.jpg")] public static var GE5: Class;
		[Embed(source = "Resource/Pictures/06.jpg")] public static var GE6: Class;
		[Embed(source = "Resource/Pictures/07.jpg")] public static var GE7: Class;
		[Embed(source = "Resource/Pictures/08.jpg")] public static var GE8: Class;
		[Embed(source = "Resource/Pictures/09.jpg")] public static var GE9: Class;
		
		[Embed(source = "Resource/Pictures/play-and-pause-button.jpg")] public static var Pause: Class;
		
	}
	
}